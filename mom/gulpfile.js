'use strict';

const gulp          = require('gulp');
const browserSync   = require('browser-sync').create();
const less          = require('gulp-less');
const watch         = require('gulp-watch');
const cleanCSS      = require('gulp-clean-css');
const imagemin 	    = require('gulp-imagemin');


const stylesPath    = './assets/styles';
const imgPath       = './assets/img';

gulp.task('styles', () => {

    return gulp.src(stylesPath + '/*.less')
        .pipe(less())
        .pipe(cleanCSS())
        .pipe(gulp.dest(stylesPath))
        .pipe(browserSync.stream());

});

gulp.task('img', () => {

    return gulp.src(imgPath + '/*')
        .pipe(imagemin())
        .pipe(gulp.dest(imgPath + '/min'));

});

gulp.task('browser-sync', () => {

    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

});

gulp.task('watch', () => {

    watch(stylesPath + '/*.less',   gulp.parallel('styles'));
    watch(imgPath + '/*',           gulp.parallel('img'));

});

gulp.task('dev', gulp.parallel('browser-sync', 'styles', 'watch'));