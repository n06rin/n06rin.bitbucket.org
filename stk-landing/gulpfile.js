var gulp = require('gulp');
var spritesmith = require('gulp.spritesmith');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var gulpCopy = require('gulp-copy');
var minify = require('gulp-minify');
var htmlmin = require('gulp-htmlmin');

gulp.task('default', function() {
  // place code for your default task here
});

//нарезает спрайты и кладет их в папку src/img,
//проблема в том что надо переименовать в сss файле адрес до спрайта
//надо будет попробовать оптимизировать это.
gulp.task('sprite', function () {
  var spriteData = gulp.src('src/sprites/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.css'
  }));
  spriteData.img.pipe(gulp.dest('src/img/')); // путь, куда сохраняем картинку
  spriteData.css.pipe(gulp.dest('src/css/')); // путь, куда сохраняем стили
});


//Объединяет файлы css. Не минифицирует их!
//для комплексного объединения и минификации надо использовать следующую функцию
gulp.task('concat-css', function (){
    return gulp.src('src/css/*.css')
        .pipe(concat('style.css'))
        .pipe(gulp.dest('www/css/'));
});

//сначала запускает предыдущую задачу, а потом минифицирует полученный файл
gulp.task('minify-css', ['concat-css'], function(){
    return gulp.src('www/css/style.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('www/css/'));
});

//копируем картинки, шрифты
gulp.task('copy-images', function(){
    return gulp.src("src/img/*")
        .pipe(gulp.dest("www/img/"));
});

gulp.task('copy-fonts', function(){
    return gulp.src("src/fonts/*")
        .pipe(gulp.dest("www/fonts/"));
});

//одна функция для вызова всего этого
gulp.task('copy-fnts-imgs', ['copy-images','copy-fonts']);

//Сначала объединяем жс, а потом минифицируем js.
gulp.task('minify-js', function() {
  gulp.src('src/scripts/*.js')
    .pipe(concat('libs.js'))
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
    }))
    .pipe(gulp.dest('www/scripts/'))
});

//минифицирую жс встроенный в страничку. Потенциально можно минифицировать и саму страницу. Но это может съедать пробелы в нужных местах
gulp.task('minify-html', function() {
  return gulp.src('src/*.html')
    .pipe(htmlmin({collapseWhitespace: false,
                   removeComments: true,
                  minifyJS: true
                  }))
    .pipe(gulp.dest('www/'));
});


gulp.task('make-release', ['minify-html', 'minify-js', 'copy-fnts-imgs', 'minify-css' ]);