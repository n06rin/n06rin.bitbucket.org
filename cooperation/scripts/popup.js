function hidePopup (e){
  e.preventDefault();
  var popup = document.getElementsByClassName("popup")[0];
  popup.classList.remove("is-visible");
}

function CreatePopupMechanic(background, closebutton){
  //background- это контейнер который содержит весь попап.

  this.show = function (e){
          if (e != undefined)
          e.preventDefault();
          background.classList.add("is-visible");
        };
  this.clickedOnBg = function (e){

          if(e.target == background)
          {
            e.preventDefault();
          background.classList.remove("is-visible");
          }
  };
  this.closeButtonClicked = function (e){
  //можно привязать закрытие попапа на нажатие кнопки
          e.preventDefault();
          if (closebutton != null && e != null){
            if(e.target == closebutton)
            //если мы знаем какой объект кнопка и она же переслала нам событие при ее нажатии то мы отключаем видимость попапа.
              background.classList.remove("is-visible");
          } else{
            //если нет того или другого, то мы все-равно закрывает попап.
            background.classList.remove("is-visible");
          }

  };
  background.addEventListener("click", this.clickedOnBg);
}
